<?php


namespace GG\Door;


interface IDoorState
{
    public function closeDoor();
    public function openDoor();
}