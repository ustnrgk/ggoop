<?php


namespace GG\Door;

require_once 'OpenState.php';

class Door
{
    private $closedState;

    private $openState;

    private $currentState;

    public function __construct()
    {
        $this->closedState = new ClosedState($this);
        $this->openState = new OpenState($this);
        $this->currentState = $this->closedState;
    }

    public function open()
    {
        $this->currentState->openDoor();
    }

    public function close()
    {
        $this->currentState->closeDoor();
    }

    public function setState(IDoorState $state)
    {
        $this->currentState = $state;
    }

    public function getState()
    {
        return $this->currentState;
    }

    public function getClosedState()
    {
        return $this->closedState;
    }

    public function getOpenState()
    {
        return $this->openState;
    }
}