<?php


namespace GG\Door;

require_once 'IDoorState.php';

class ClosedState implements IDoorState
{
    private $door;

    public function __construct(Door $door)
    {
        $this->door = $door;
    }

    public function closeDoor()
    {
        echo 'The door is already closed. Open it.';
    }

    public function openDoor()
    {
        echo 'Door opened.';
        $this->door->setState($this->door->getOpenState());
    }
}