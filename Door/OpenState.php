<?php


namespace GG\Door;

require_once 'IDoorState.php';

class OpenState implements IDoorState
{
    private $door;

    public function __construct(Door $door)
    {
        $this->door = $door;
    }

    public function closeDoor()
    {
        echo 'Door closed.';
        $this->door->setState($this->door->getClosedState());
    }

    public function openDoor()
    {
        echo 'The door is already opened. Close it.';
    }
}