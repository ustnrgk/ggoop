<?php


namespace GG\DrinkType;

require_once 'IDrinkType.php';

class Cola33 implements IDrinkType
{
    private $maxFillAbleToShelf;

    public function __construct()
    {
        $this->maxFillAbleToShelf = 20;
    }

    public function getMaxFillAbleToShelf()
    {
        return $this->maxFillAbleToShelf;
    }

    public function __toString()
    {
        return 'Cola33';
    }
}