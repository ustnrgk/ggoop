<?php


namespace GG\DrinkType;


interface IDrinkType
{
    public function getMaxFillAbleToShelf();
}