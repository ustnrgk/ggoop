<?php


namespace GG\Shelf;


interface IShelfState
{
    public function setFullState();
    public function setHalfFullState();
    public function setEmptyState();
}