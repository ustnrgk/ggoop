<?php


namespace GG\Shelf;

require_once 'IShelfState.php';

class FullState implements IShelfState
{
    private $shelf;

    public function __construct(Shelf $shelf)
    {
        $this->shelf = $shelf;
    }

    public function setFullState()
    {
        return;
    }

    public function setHalfFullState()
    {
        $this->shelf->setState($this->shelf->getHalfFullState());
    }

    public function setEmptyState()
    {
        $this->shelf->setState($this->shelf->getEmptyState());
    }
}