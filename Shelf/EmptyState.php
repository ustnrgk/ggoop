<?php


namespace GG\Shelf;

require_once 'IShelfState.php';

class EmptyState implements IShelfState
{
    private $shelf;

    public function __construct(Shelf $shelf)
    {
        $this->shelf = $shelf;
    }

    public function setFullState()
    {
        $this->shelf->setState($this->shelf->getFullState());
    }

    public function setHalfFullState()
    {
        $this->shelf->setState($this->shelf->getHalfFullState());
    }

    public function setEmptyState()
    {
        return;
    }
}