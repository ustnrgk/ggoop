<?php


namespace GG\Shelf;

require_once 'FullState.php';
require_once 'EmptyState.php';
require_once 'HalfFullState.php';

use GG\DrinkType\Cola33;

class Shelf
{
    private $currentDrinkCount;

    private $fullState;

    private $emptyState;

    private $halfFullState;

    private $currentState;

    public function __construct()
    {
        $this->fullState = new FullState($this);
        $this->emptyState = new EmptyState($this);
        $this->halfFullState = new HalfFullState($this);

        $this->currentState = $this->emptyState;
    }

    public function putDrink(Cola33 $drink)
    {
        if ($this->currentState instanceof FullState) {
            return false;
        }

        $this->currentDrinkCount++;

        if ($this->currentDrinkCount == $drink->getMaxFillAbleToShelf()) {
            $this->currentState->setFullState();
        } else {
            $this->currentState->setHalfFullState();
        }

        return true;
    }

    public function getDrink()
    {
        if ($this->currentState instanceof EmptyState) {
            return false;
        }

        $this->currentDrinkCount--;

        if ($this->currentDrinkCount == 0) {
            $this->currentState->setEmptyState();
        } else {
            $this->currentState->setHalfFullState();
        }

        return new Cola33();
    }

    public function getFullState()
    {
        return $this->fullState;
    }

    public function getHalfFullState()
    {
        return $this->halfFullState;
    }

    public function getEmptyState()
    {
        return $this->emptyState;
    }

    public function setState(IShelfState $state)
    {
        $this->currentState = $state;
    }

    public function getState()
    {
        return $this->currentState;
    }
}