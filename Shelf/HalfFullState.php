<?php


namespace GG\Shelf;

require_once 'IShelfState.php';

class HalfFullState implements IShelfState
{
    private $shelf;

    public function __construct(Shelf $shelf)
    {
        $this->shelf = $shelf;
    }

    public function setFullState()
    {
        $this->shelf->setState($this->shelf->getFullState());
    }

    public function setHalfFullState()
    {
        return;
    }

    public function setEmptyState()
    {
        $this->shelf->setState($this->shelf->getEmptyState());
    }
}