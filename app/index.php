<?php

require_once '../Fridge/Fridge.php';

use GG\Fridge\Fridge;

$fridge = new Fridge();

$fridge->openDoor();
echo '<br>';
$fridge->putDrink();
$fridge->closeDoor();
echo '<br>';
$fridge->openDoor();
echo '<br>';
$fridge->putDrink();

$drink = $fridge->getDrink();

echo 'You got a ' . $drink . '.';
echo '<br>';
echo $fridge->getState();