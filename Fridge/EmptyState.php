<?php


namespace GG\Fridge;

require_once 'IFridgeState.php';

class EmptyState implements IFridgeState
{
    private $fridge;

    public function __construct(Fridge $fridge)
    {
        $this->fridge = $fridge;
    }

    public function setFullState()
    {
        $this->fridge->setState($this->fridge->getFullState());
    }

    public function setHalfFullState()
    {
        $this->fridge->setState($this->fridge->getHalfFullState());
    }

    public function setEmptyState()
    {
        return;
    }

    public function __toString()
    {
        return 'Fridge is empty';
    }
}