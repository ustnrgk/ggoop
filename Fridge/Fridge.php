<?php


namespace GG\Fridge;

require_once '../Door/Door.php';
require_once '../Door/ClosedState.php';
require_once '../Shelf/Shelf.php';
require_once 'EmptyState.php';
require_once 'HalfFullState.php';
require_once 'FullState.php';
require_once '../DrinkType/Cola33.php';

use GG\Door\Door;
use GG\DrinkType\Cola33;
use GG\Shelf\Shelf;
use GG\Door\OpenState as DoorOpenState;
use GG\Shelf\EmptyState as ShelfEmptyState;
use GG\Shelf\HalfFullState as ShelfHalfFullState;
use GG\Shelf\FullState as ShelfFullState;

class Fridge
{

    private $door;

    private $shelfs;

    private $drinkGot;

    private $drinkPut;

    private $emptyState;

    private $halfFullState;

    private $fullState;

    private $currentState;

    public function __construct()
    {
        $this->door = new Door();
        $this->shelfs = [
            new Shelf(),
            new Shelf(),
            new Shelf(),
        ];

        $this->drinkGot = 0;
        $this->drinkPut = 0;

        $this->emptyState = new EmptyState($this);
        $this->halfFullState = new HalfFullState($this);
        $this->fullState = new FullState($this);

        $this->currentState = $this->emptyState;
    }

    public function openDoor()
    {
        $this->door->open();
    }

    public function closeDoor()
    {
        $this->drinkGot = 0;
        $this->drinkPut = 0;

        $this->door->close();
    }

    public function getDrink()
    {
        if (!$this->door->getState() instanceof DoorOpenState) {
            echo 'Open the door first!';
            return null;
        }

        if ($this->currentState instanceof EmptyState) {
            echo 'The fridge is empty.';
            return null;
        }

        if ($this->drinkGot > 0) {
            echo 'You almost got a drink. Close the door.';
            return null;
        }

        $drink = null;

        /** @var Shelf $shelf */
        foreach ($this->shelfs as $shelf) {

            if (!$shelf->getState() instanceof ShelfEmptyState) {
                $this->drinkGot++;
                $drink = $shelf->getDrink();

                break;
            }

        }

        $this->updateState();

        return $drink;

    }

    public function putDrink()
    {
        if (!$this->door->getState() instanceof DoorOpenState) {
            echo 'Open the door first!';
            return null;
        }

        if ($this->currentState instanceof FullState) {
            echo 'The fridge is full.';
            return null;
        }

        if ($this->drinkPut > 0) {
            echo 'You almost put a drink. Close the door.';
            return null;
        }

        $drinkPut = false;

        /** @var Shelf $shelf */
        foreach ($this->shelfs as $shelf) {

            if (!$shelf->getState() instanceof ShelfFullState) {
                $this->drinkPut++;
                $drinkPut = $shelf->putDrink(new Cola33());

                break;
            }

        }

        $this->updateState();

        return $drinkPut;
    }

    private function updateState()
    {
        $fullStateShelfCount = 0;
        $emptyStateShelfCount = 0;

        /** @var Shelf $shelf */
        foreach ($this->shelfs as $shelf) {
            if ($shelf->getState() instanceof ShelfHalfFullState) {
                $this->currentState = $this->halfFullState;
                return;
            }

            if ($shelf->getState() instanceof ShelfEmptyState) {
                $emptyStateShelfCount++;
            }

            if ($shelf->getState() instanceof ShelfFullState) {
                $fullStateShelfCount++;
            }
        }

        if ($fullStateShelfCount == 0) {
            $this->currentState = $this->emptyState;
            return;
        }

        if ($emptyStateShelfCount == 0) {
            $this->currentState = $this->fullState;
            return;
        }

        $this->currentState = $this->halfFullState;

        return;
    }

    public function getFullState()
    {
        return $this->fullState;
    }

    public function getHalfFullState()
    {
        return $this->halfFullState;
    }

    public function getEmptyState()
    {
        return $this->emptyState;
    }

    public function setState(IFridgeState $state)
    {
        $this->currentState = $state;
    }

    public function getState()
    {
        return $this->currentState;
    }
}