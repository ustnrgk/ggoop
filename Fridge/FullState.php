<?php


namespace GG\Fridge;


class FullState implements IFridgeState
{
    private $fridge;

    public function __construct(Fridge $fridge)
    {
        $this->fridge = $fridge;
    }

    public function setFullState()
    {
        return;
    }

    public function setHalfFullState()
    {
        $this->fridge->setState($this->fridge->getHalfFullState());
    }

    public function setEmptyState()
    {
        $this->fridge->setState($this->fridge->getEmptyState());
    }

    public function __toString()
    {
        return 'Fridge is full';
    }
}