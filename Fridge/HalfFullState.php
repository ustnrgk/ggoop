<?php


namespace GG\Fridge;


class HalfFullState implements IFridgeState
{
    private $fridge;

    public function __construct(Fridge $fridge)
    {
        $this->fridge = $fridge;
    }

    public function setFullState()
    {
        $this->fridge->setState($this->fridge->getFullState());
    }

    public function setHalfFullState()
    {
        return;
    }

    public function setEmptyState()
    {
        $this->fridge->setState($this->fridge->getEmptyState());
    }

    public function __toString()
    {
        return 'Fridge is half-full';
    }
}