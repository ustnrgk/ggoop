<?php


namespace GG\Fridge;


interface IFridgeState
{
    public function setFullState();
    public function setHalfFullState();
    public function setEmptyState();
}